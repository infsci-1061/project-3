﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    private Transform target;
    NavMeshAgent ai;
    public GameManager gm;
    public ProjectileBehavior projectile;
    Ray path;

    // Start is called before the first frame update
    void Start()
    {
        if (gm == null)
            gm = GameManager.instance;
        ai = gameObject.GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        // Fires the weapon once every 3 seconds after
        // the initial first second of gameplay
        InvokeRepeating("Shoot", 1f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        ai.destination = target.position;
        path = new Ray(transform.position, ai.destination);
    }

    // Fires the enemy weapon only when the AI is moving
    void Shoot()
    {
        if (!ai.isStopped)
        {
            ProjectileBehavior enemyProjectile = Instantiate(projectile.gameObject).GetComponent<ProjectileBehavior>();
            enemyProjectile.transform.position = GameObject.Find("Launcher").transform.position;
            enemyProjectile.transform.rotation = GameObject.Find("Launcher").transform.rotation;
            enemyProjectile.SetDirection(path.direction);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("Game Projectile Variant(Clone)"))
            StartCoroutine(Pause());
    }
    
    // Pause the enemy movement when the character is hit with
    // the appropriate projectile
    IEnumerator Pause ()
    {
        ai.isStopped = true;
        yield return new WaitForSecondsRealtime(3);
        ai.isStopped = false;
    }
}
