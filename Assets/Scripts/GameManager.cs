﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static int health, aiHealth, primaryDamage, secondaryDamage, enemyDamage;
    private static int primaryAmmo, secondaryAmmo;
    public static GameManager instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        aiHealth = health = 30;
        primaryDamage = 1;
        secondaryDamage = 3;
        enemyDamage = 6;
        primaryAmmo = 10;
        secondaryAmmo = 5;
    }

    public int getHealth()
    {
        return health;
    }
    
    public void decreaseHealth(GameObject Projectile)
    {
        if (Projectile.name.Equals("Projectile(Clone)"))
            aiHealth -= primaryDamage;
        if (Projectile.name.Equals("Game Projectile Variant(Clone)") || Projectile.name.Equals("Beta Projectile Variant(Clone)"))
            aiHealth -= secondaryDamage;
        if (Projectile.name.Equals("Enemy Projectile Variant(Clone)"))
            health -= enemyDamage;
    }

    public int getAIHealth()
    {
        return aiHealth;
    }

    public int getPrimaryAmmo()
    {
        return primaryAmmo;
    }

    public int getSecondaryAmmo()
    {
        return secondaryAmmo;
    }

    public void decreasePrimaryAmmo()
    {
        primaryAmmo--;
    }

    public void decreaseSecondaryAmmo()
    {
        secondaryAmmo--;
    }

    public void reloadPrimary()
    {
            primaryAmmo = 10;
    }

    public void reloadSecondary()
    {
        secondaryAmmo = 5;
    }
}