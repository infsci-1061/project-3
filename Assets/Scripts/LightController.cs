﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    public GameObject player, playerv;

    // Controls the light to add a spotlight effect on the player
    void Update()
    {
        if (player.activeInHierarchy)
            transform.LookAt(player.GetComponent<Transform>());
        if (playerv.activeInHierarchy)
            transform.LookAt(playerv.GetComponent<Transform>());
    }
}
