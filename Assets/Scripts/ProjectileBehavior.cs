﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour
{
    GameManager gm;
    Rigidbody rb;
    public float speed;
    bool sent;
    Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        if (gm == null)
            gm = GameManager.instance;
        rb = GetComponent<Rigidbody>();
    }

    public void SetDirection (Vector3 dir)
    {
        direction = dir;
        sent = true;
    }

    private void FixedUpdate()
    {
        if (sent)
        {
            rb.AddForce(direction * speed);
            sent = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Let Beta projectiles pass through obstacles
        if (gameObject.name.Equals("Beta Projectile Variant(Clone)"))
            Physics.IgnoreLayerCollision(9, 10);
        // Decrease the AI health if it
        if (collision.gameObject.CompareTag("Enemy") &&
            (gameObject.name.Equals("Projectile(Clone)") || 
            gameObject.name.Equals("Beta Projectile Variant(Clone)") || 
            gameObject.name.Equals("Game Projectile Variant(Clone)")))
            gm.decreaseHealth(gameObject);
        // Decrease the Player health if hit
        if (gameObject.name.Equals("Enemy Projectile Variant(Clone)") && collision.gameObject.CompareTag("Player"))
            gm.decreaseHealth(gameObject);
        Destroy(gameObject);
    }

    private void OnCollisionStay(Collision collision)
    {
        // Destroy the Beta projectile if it touches the map
        if (collision.gameObject.CompareTag("Map"))
            Destroy(gameObject);
    }
}
