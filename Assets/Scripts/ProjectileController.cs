﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    public ProjectileBehavior primary, secondary;
    public GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        if (gm == null)
            gm = GameManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        // Instantiates a projectile and moves it along the ray
        // gotten from the mouse
        if (Input.GetMouseButtonDown(0) && gm.getPrimaryAmmo() > 0)
        {
            Ray path = Camera.main.ScreenPointToRay(Input.mousePosition);
            ProjectileBehavior primProjectile = Instantiate(primary.gameObject).GetComponent<ProjectileBehavior>();
            primProjectile.transform.position = GameObject.Find("primLauncher").transform.position;
            primProjectile.transform.rotation = GameObject.Find("primLauncher").transform.rotation;
            primProjectile.SetDirection(path.direction);
            gm.decreasePrimaryAmmo();
        }

        if (Input.GetMouseButtonDown(1) && gm.getSecondaryAmmo() > 0)
        {
            Ray path = Camera.main.ScreenPointToRay(Input.mousePosition);
            ProjectileBehavior sndProjectile = Instantiate(secondary.gameObject).GetComponent<ProjectileBehavior>();
            sndProjectile.transform.position = GameObject.Find("sndLauncher").transform.position;
            sndProjectile.transform.rotation = GameObject.Find("sndLauncher").transform.rotation;
            sndProjectile.SetDirection(path.direction);
            gm.decreaseSecondaryAmmo();
        }
    }
}
