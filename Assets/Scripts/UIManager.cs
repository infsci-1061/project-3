﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text healthText, primText, sndText, enemyHealthText;
    public GameObject hud, over, win, primCounter, sndCounter;
    public Texture2D crosshair;
    private Rect position;
    public GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        if (gm == null)
            gm = GameManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        // Variables to display the current health and ammo counters
        healthText.text = "Health: " + gm.getHealth();
        primText.text = "Primary: " + gm.getPrimaryAmmo();
        sndText.text = "Secondary: " + gm.getSecondaryAmmo();
        enemyHealthText.text = "Enemy Health: " + gm.getAIHealth();

        // Display and hold crosshairs on screen
        position = new Rect((Screen.width - crosshair.width) / 2, (Screen.height - crosshair.height) / 2, crosshair.width, crosshair.height);

        // Refill counter when refilling primary ammo
        if (gm.getPrimaryAmmo() == 0)
        {
            primCounter.SetActive(true);
            primCounter.GetComponent<Image>().fillAmount -= 1.0f / 2.0f * Time.fixedDeltaTime;
            primText.text = "Primary:";
        }

        if (primCounter.GetComponent<Image>().fillAmount == 0)
        {
            primCounter.SetActive(false);
            primCounter.GetComponent<Image>().fillAmount = 1f;
            gm.reloadPrimary();
            primText.text = "Primary: " + gm.getPrimaryAmmo();
        }

        // Refill counter when refilling secondary ammo
        if (gm.getSecondaryAmmo() == 0)
        {
            sndCounter.SetActive(true);
            sndCounter.GetComponent<Image>().fillAmount -= 1.0f / 5.0f * Time.fixedDeltaTime;
            sndText.text = "Secondary:";
        }

        if (sndCounter.GetComponent<Image>().fillAmount == 0)
        {
            sndCounter.SetActive(false);
            sndCounter.GetComponent<Image>().fillAmount = 1f;
            gm.reloadSecondary();
            sndText.text = "Secondary: " + gm.getSecondaryAmmo();
        }

        // Display "Game Over" and quit when Esc is pressed
        if (gm.getHealth() <= 0)
        {
            over.SetActive(true);
            hud.SetActive(false);
            if (Input.GetKey("escape"))
                Application.Quit();
        }

        // Display "You Win" and quit when Esc is pressed
        if (gm.getAIHealth() <= 0)
        {
            win.SetActive(true);
            hud.SetActive(false);
            if (Input.GetKey("escape"))
                Application.Quit();
        }
    }
    void OnGUI()
    {
        GUI.DrawTexture(position, crosshair);
    }
}
