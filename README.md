# Rental Wars

Rental Wars lets you battle an evil Netflix animatron in the waning days of your friendly neighborhood rental store's life.
Avoid enemy Internet packets, and kill the evil Lord Netflix to save the days of forgetting to return that VHS tape you lost under the couch.

# Game Features
* First Person shooter built in the Unity engine
* 2 different characters with different secondary weapon abilities
* Soundtrack inspired by classic thrash metal of the 90's

# Requirements Summary
* A Navmesh is used to make the AI follow the player
* Because this is an arena shooter in the style of Unreal Tournament,
  the level is one room with a number of obstacles for cover
* Since this involves movie rental companies, the entire arena is made of a
  movie theater carpet texture
* Both player characters have secondary weapons with differing effects to make
  gameplay more diverse
* The three assets from the Asset store are the Unity Standard Assets, the Unity
  Particle Pack, and Epic Rock Track music
  * The Standard Assets are used to control the camera and player movements
  * The Particle Pack is used to give a rocket trail effect to the projectiles
  * The Epic Rock Track is used to provide the soundtrack for the game
* The UI components consist of text displaying the AI Health, Player Health,
  remaining ammo for the player, and a crosshair to assist with aiming
  * When the player runs out of ammo, a reload countdown is displayed

# [Itch.io download](https://cjp70.itch.io/rental-wars?secret=U9Or8Ef5CVBXiGBP4TSHg7FBYFQ)